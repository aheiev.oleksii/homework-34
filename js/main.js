const path = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
const img = document.createElement('img');

function randomImg(path) {
    for (let i = 1; i < path.length; i++) {
        return random(path.length, i);
    }
}

function random(max, min) {
    return Math.floor(Math.random() * (max - min) + min);
}

img.setAttribute('src', `img/${randomImg(path)}.jpg`);
img.setAttribute('width', '700');

document.body.append(img);